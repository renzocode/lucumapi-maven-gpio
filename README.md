# lucumapi-maven-gpio

:point_up: note: you should install maven.

Clone lucumapi-maven-gpio repository
```bash
$ git clone git@gitlab.com:renzocode/lucumapi-maven-gpio.git

```
go to repository lucumapi-maven-gpio

```bash
$ cd lucumapi-maven-gpio
```
change master to develop branch 

```bash
$ git checkout develop
```

Download GPIO package and copy Maven repository dependency to POM.xml

 [GPIO](https://pi4j.com/1.2/download.html "GPIO")


Compile Lucumapi-maven-gpio

```bash
$ mvn compile
```

execute Lucumapi-maven-gpio

```bash
$ mvn exec:java -Dexec.mainClass="com.project01.lucumapi.main.App"
```

